from django.shortcuts import render
from datetime import datetime, date
from .models import DataProfil

# Create your views here.
profile_name = 'Hepzibah Smith' #TODO implement your name here
birth_date = date(1998,1,1) #TODO implement your birthday
birthdate = birth_date.strftime('%d %B %Y')
gender = 'Female' #TODO implement your gender
email = 'hello@smith.com' #TODO implement your email
desc_profile = "Antique expert. Experience as marketer for 10 years"
#TODO implement your expertise minimal 3
expert = ["Marketing", "Public Speaking", "Collector"]

def index(request):
	if(DataProfil.objects.count()==0):
		profil = DataProfil(name = profile_name, Birthday = birthdate, Gender= gender, Email = email, Description=desc_profile, expertise=expert)
		profil.save();
	profil = DataProfil.objects.all()[0]
	expertise = [ s[1:-1] for s in profil.expertise[1:-1].split(", ") ]
	response = {'name' : profil.name, 'Birthday' : profil.Birthday, 'Gender': profil.Gender, 'expertise':expertise, 'Description' : profil.Description, 'Email': profil.Email}
	html = 'profil.html'
	return render(request, html, response)	


