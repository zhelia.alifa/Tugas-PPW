from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message


response = {'author': "Hauri Silmi"} #TODO Implement yourname
def index(request):
	html = 'add_friend/add_friend.html'
	response['message_form'] = Message_Form
	# if (Message.objects.count() == 0):
	data = Message.objects.all()
	response['data'] = data
	return render(request, html, response)
	
def add_friend(request):
	form = Message_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name'] 
		response['heroku_url'] = request.POST['heroku_url'] 
		data = Message(name=response['name'], heroku_url=response['heroku_url'])
		data.save()
		return HttpResponseRedirect('/add-friend/')
	else:
		return HttpResponseRedirect('/add-friend/')