from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend
from .models import Message
from .forms import Message_Form

# Create your tests here.
 
class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code, 200)

	def test_addFriend_using_index_func(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)

	def test_form_message_input_has_placeholder_and_css_classes(self):
		form = Message_Form()
		self.assertIn('class="form-control"', form.as_p())
		self.assertIn('<label for="id_name">Nama:</label>', form.as_p())
		self.assertIn('<label for="id_heroku_url">Heroku URL:</label>', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Message_Form(data={'name': '', 'heroku_url': ''})
		self.assertFalse(form.is_valid())

	def test_add_friend_fail(self):
		response = Client().post('/add-friend/add_friend', {'name': 'Anonymous', 'heroku_url': 'http://www.google.com'})
		self.assertEqual(response.status_code, 302)

	def test_add_friend_showing_all_friend(self):
		name = 'Hauri'
		heroku_url = 'http://www.google.com'
		data = {'name':name, 'heroku_url':heroku_url}
		post_data = Client().post('/add-friend/', data)
		self.assertEqual(post_data.status_code, 200) 