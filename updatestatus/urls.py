from django.conf.urls import url
from .views import index, add_todo, delete_status

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_todo', add_todo, name='add_todo'),
	url(r'^delete_status/(?P<id_status>[0-9]+)', delete_status, name = 'delete_status'),
 ]
