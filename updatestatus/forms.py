from django import forms

class Todo_Form(forms.Form):
	error_message = {
		'required': 'Tolong isi update status ini',
	}

	description_attrs = {
		'type': 'text',
		'cols': 100,
		'rows': 4,
		'class': 'todo-form-textarea'
	}

	description = forms.CharField(label='',required=True, widget=forms.Textarea(attrs=description_attrs))