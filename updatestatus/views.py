from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo
from profil.views import *
from profil.models import DataProfil

# Create your views here.
response={}

def index(request):
    todo = Todo.objects.order_by('-id')
    response['todo'] = todo
    html = 'updatestatus/update_status.html'
    response['todo_form'] = Todo_Form
    response['name'] = profile_name
    if(DataProfil.objects.count()==0):
        profil = DataProfil(name = profile_name, Birthday = birthdate, Gender= gender, Email = email, Description=desc_profile, expertise=expert)
        profil.save();
    nama = DataProfil.objects.get(pk=1)
    response['author'] = nama.name
    return render(request, html, response)


def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        todo = Todo(description=response['description'])
        todo.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')

def delete_status(request, id_status):
    status = Todo.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/update-status/')
