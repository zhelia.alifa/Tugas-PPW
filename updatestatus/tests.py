from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_todo, delete_status
from .models import Todo
from .forms import Todo_Form
from django.http import HttpRequest

class UpdateStatusUnitTest(TestCase):

    def test_update_status_url_is_exist(self):
        respone = Client().get('/update-status/')
        self.assertEqual(respone.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_update_status(self):
        #Creating a new activity
        new_activity= Todo.objects.create(description='mengerjakan tugas 1 ppw')

        #Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_updateStatus_input_has_placeholder_and_css_classes(self):
        form = Todo_Form()
        #self.assertIn('class="todo-form-input', form.as_p())
        #self.assertIn('id="id_title"', form.as_p())
        self.assertIn('class="todo-form-textarea', form.as_p())     
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_updateStatus_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/add_todo', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_updateStatus_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/add_todo', {'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_delete_status_working(self):
        test_delete = Todo.objects.create(description='try to delete this')
        request = HttpRequest()
        id = test_delete.id
        response = delete_status(request, id)
        self.assertEqual(response.status_code, 302)
