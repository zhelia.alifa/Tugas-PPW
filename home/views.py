from django.shortcuts import render 
from add_friend.models import Message
from updatestatus.models import Todo
from profil.models import DataProfil
from datetime import datetime, date
# Create your views here.
response = {}
def index(request):
    profile_name = 'Hepzibah Smith' #TODO implement your name here
    birth_date = date(1998,1,1) #TODO implement your birthday
    birthdate = birth_date.strftime('%d %B %Y')
    gender = 'Female' #TODO implement your gender
    email = 'hello@smith.com' #TODO implement your email
    desc_profile = "Antique expert. Experience as marketer for 10 years"
    #TODO implement your expertise minimal 3
    expert = ["Marketing", "Public Speaking", "Collector"]
    response['author']="Js"
    html = 'home/statistik.html'
    if( DataProfil.objects.all().count() == 0  ): 
        profil = DataProfil(name = profile_name, Birthday = birthdate, Gender= gender, Email = email, Description=desc_profile, expertise=expert)
        profil.save();
    nama = DataProfil.objects.get( pk=1 )
    response['name'] = nama.name
    response['friends'] = Message.objects.all().count()
    response['post'] = Todo.objects.all().count()
    if(Todo.objects.all().count() == 0):
       response['description'] = "No Post" 
    else:
        last = Todo.objects.all()[Todo.objects.all().count() - 1]
        response['description'] = last.description
        response['date'] = last.created_date

    return render(request, html, response)



