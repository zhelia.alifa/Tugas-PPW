from django.test import TestCase
from django.test import Client
from django.urls import resolve

from add_friend.models import Message
from updatestatus.models import Todo
from profil.models import DataProfil
from django.utils import timezone
from .views import index
from django.http import HttpRequest
# Create your tests here.
class HomeUnitTest(TestCase):
    def test_friends_statistics(self):
        new_friend = Message.objects.create(name="haha",heroku_url="https://riokurtinus@herokuapps.com",created_date=timezone.now())
        friends = Message.objects.all().count()
        self.assertEqual(friends ,1 )           
    def test_post_statistics(self):
        new_post = Todo.objects.create(description="tes",created_date=timezone.now()) 
        posts = Todo.objects.all().count()
        self.assertEqual(posts ,1)
    def test_latest_post_is_latest(self):
        last = "wohoo"
        latest = Todo.objects.create(description=last,created_date=timezone.now())
        postinganteratas = Todo.objects.last()
        self.assertEqual(postinganteratas.description ,last)
    def test_navbar(self):
        data = DataProfil(name="nama",Birthday="1990-03-02" , Gender="boy", expertise="expert",Description="description",Email="jt@gmail.com" )
        data.save()
        todo = Todo(description="tes",created_date=timezone.now())
        todo.save()
        response = Client().get('/home/')
        html_response = response.content.decode('utf8')
        self.assertIn('navbar',html_response)
    def test_lab_4_url_is_exist(self):
        data = DataProfil(name="nama",Birthday="1990-03-02", Gender="boy", expertise="expert",Description="description",Email="jt@gmail.com" )
        data.save()
        todo = Todo(description="tes",created_date=timezone.now())
        todo.save()
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_lab4_using_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)

    def test_root_url_now_is_using_index_page__from_home(self):
        data = DataProfil(name="nama",Birthday="1990-03-02", Gender="boy", expertise="expert",Description="description",Email="jt@gmail.com" )
        data.save()
        todo = Todo(description="tes",created_date=timezone.now())
        todo.save()
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/home/',301,200)

       