"""TUGAS1PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import updatestatus.urls as updatestatus
import add_friend.urls as add_friend
import profil.urls as profil
import home.urls as home


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update-status/', include(updatestatus, namespace='update-status')),
    url(r'^add-friend/', include(add_friend,namespace='add-friend')),
    url(r'^home/', include(home,namespace='home')),
    url(r'^profil/', include(profil,namespace='profil')),
    url(r'^$', RedirectView.as_view(url='/home/',permanent=True), name='redirect_landing_page'),

]
